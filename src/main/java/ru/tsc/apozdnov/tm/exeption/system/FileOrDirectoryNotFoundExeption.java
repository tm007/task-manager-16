package ru.tsc.apozdnov.tm.exeption.system;

import ru.tsc.apozdnov.tm.exeption.AbstractExeption;

public final class FileOrDirectoryNotFoundExeption extends AbstractExeption {

    public FileOrDirectoryNotFoundExeption() {
        super("FAULT!! File or Directory does not exist!!!!");
    }

}
