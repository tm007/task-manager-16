package ru.tsc.apozdnov.tm.exeption.system;

import ru.tsc.apozdnov.tm.exeption.AbstractExeption;

public final class CommandNotSupportedExeption extends AbstractExeption {

    public CommandNotSupportedExeption() {
        super("FAULT!!! Command Not Supported");
    }

    public CommandNotSupportedExeption(final String command) {
        super("FAULT!!!" + command + " is not supported!");
    }

}
