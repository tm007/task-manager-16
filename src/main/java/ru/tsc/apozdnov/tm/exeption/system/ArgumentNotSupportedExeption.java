package ru.tsc.apozdnov.tm.exeption.system;

import ru.tsc.apozdnov.tm.exeption.AbstractExeption;

public final class ArgumentNotSupportedExeption extends AbstractExeption {

    public ArgumentNotSupportedExeption() {
        super("FAULT!! Argument Not Supported");
    }

    public ArgumentNotSupportedExeption(final String arg) {
        super("FAULT!!! Argument " + arg + " is not supported!");
    }

}
