package ru.tsc.apozdnov.tm.exeption.field;

public final class IndexIncorrectExeption extends AbstractFieldExeption {

    public IndexIncorrectExeption() {
        super("FAULT! Index is EMPTY!!!");
    }

}
