package ru.tsc.apozdnov.tm.controller;

import ru.tsc.apozdnov.tm.api.controller.ITaskController;
import ru.tsc.apozdnov.tm.api.service.ITaskService;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.model.Task;
import ru.tsc.apozdnov.tm.util.DateUtil;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void renderTasks(final List<Task> taskList) {
        int index = 1;
        for (final Task task : taskList) {
            if (task == null) continue;
            System.out.println(index + "." + task);
            index++;
        }
    }

    @Override
    public void showTaskListByProjectId() {
        System.out.println("**** TASK LIST BY PROJECT ID ****");
        System.out.println("ENTER ROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        renderTasks(tasks);
    }

    @Override
    public void showTaskList() {
        System.out.println("**** TASK LIST ****");
        System.out.println("**** ENTER SORT ****");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = taskService.findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public void clearTasks() {
        System.out.println("**** TASK CLEAR ****");
        taskService.clear();
    }

    @Override
    public void createTask() {
        System.out.println("**** TASK CREATE ****");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("***** REMOVE TASK BY INDEX ****");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeByIndex(index);
    }

    @Override
    public void removeTaskById() {
        System.out.println("***** REMOVE TASK BY ID ****");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("***** SHOW TASK BY INDEX ****");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        showTask(task);
    }

    @Override
    public void showTaskById() {
        System.out.println("***** SHOW TASK BY ID ****");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        showTask(task);
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("***** UPDATE TASK BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index, name, description);
    }

    @Override
    public void updateTaskById() {
        System.out.println("***** UPDATE TASK BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("***** CHANGE TASK STATUS BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusById(id, status);
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("***** CHANGE TASK STATUS BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusByIndex(index, status);
    }

    @Override
    public void startTaskById() {
        System.out.println("***** START TASK BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("***** START TASK BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeTaskById() {
        System.out.println("***** COMPLETE TASK BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("***** COMPLETE TASK BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescriprion());
        System.out.println("ID: " + task.getId());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getDateCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        final Status status = task.getStatus();
        if (status != null)
            System.out.println("STATUS: " + status.getDisplayName());
    }

}
