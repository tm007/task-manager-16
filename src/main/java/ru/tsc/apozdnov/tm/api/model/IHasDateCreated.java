package ru.tsc.apozdnov.tm.api.model;

import java.util.Date;

public interface IHasDateCreated {

    Date getDateCreated();

    void setDateCreated(Date dateCreated);

}
