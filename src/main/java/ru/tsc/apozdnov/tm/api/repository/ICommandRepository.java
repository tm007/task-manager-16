package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
